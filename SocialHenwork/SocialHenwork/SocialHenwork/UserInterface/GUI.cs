﻿using MetroFramework.Forms;
using SocialHenwork.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocialHenwork.UserInferface
{
    public partial class GUI : MetroForm
    {
        public GUI()
        {
            InitializeComponent();

            //De base on est sur twitter donc pas de nom de post
            metroComboBoxSocialNetwork.SelectedIndex = 0;
            metroTextBoxPostName.Enabled = false;
            metroTextBoxSerializedPosts.Text = Static.TWITTER.GetSerializedPosts();
        }

        private void RefreshDatas()
        {
            if (metroComboBoxSocialNetwork.SelectedIndex == 0)
                metroTextBoxSerializedPosts.Text = Static.TWITTER.GetSerializedPosts();
        }

        private void metroButtonPublish_Click(object sender, EventArgs e)
        {
            if (metroComboBoxSocialNetwork.SelectedIndex == 0)
            {
                Static.TWITTER.Publish(metroTextBoxPostContent.Text);
                RefreshDatas();
            }
        }

        private void metroRadioButtonXML_CheckedChanged(object sender, EventArgs e)
        {
            if (metroRadioButtonXML.Checked)
                metroRadioButtonJSON.Checked = false;
            else
                metroRadioButtonJSON.Checked = true;
            Static.USING_XML = !metroRadioButtonJSON.Checked;
            RefreshDatas();
        }
    }
}
