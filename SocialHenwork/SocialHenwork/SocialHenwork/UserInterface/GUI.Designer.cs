﻿namespace SocialHenwork.UserInferface
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroComboBoxSocialNetwork = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroRadioButtonXML = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButtonJSON = new MetroFramework.Controls.MetroRadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroButtonPublish = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxPostContent = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxPostName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxSerializedPosts = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroComboBoxSocialNetwork
            // 
            this.metroComboBoxSocialNetwork.FormattingEnabled = true;
            this.metroComboBoxSocialNetwork.ItemHeight = 23;
            this.metroComboBoxSocialNetwork.Items.AddRange(new object[] {
            "Twitter"});
            this.metroComboBoxSocialNetwork.Location = new System.Drawing.Point(134, 83);
            this.metroComboBoxSocialNetwork.Name = "metroComboBoxSocialNetwork";
            this.metroComboBoxSocialNetwork.Size = new System.Drawing.Size(306, 29);
            this.metroComboBoxSocialNetwork.TabIndex = 0;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(26, 87);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(103, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Social Network :";
            // 
            // metroRadioButtonXML
            // 
            this.metroRadioButtonXML.AutoSize = true;
            this.metroRadioButtonXML.Checked = true;
            this.metroRadioButtonXML.Location = new System.Drawing.Point(587, 27);
            this.metroRadioButtonXML.Name = "metroRadioButtonXML";
            this.metroRadioButtonXML.Size = new System.Drawing.Size(135, 15);
            this.metroRadioButtonXML.TabIndex = 2;
            this.metroRadioButtonXML.TabStop = true;
            this.metroRadioButtonXML.Text = "Use XML Serialization";
            this.metroRadioButtonXML.UseVisualStyleBackColor = true;
            this.metroRadioButtonXML.CheckedChanged += new System.EventHandler(this.metroRadioButtonXML_CheckedChanged);
            // 
            // metroRadioButtonJSON
            // 
            this.metroRadioButtonJSON.AutoSize = true;
            this.metroRadioButtonJSON.Location = new System.Drawing.Point(734, 27);
            this.metroRadioButtonJSON.Name = "metroRadioButtonJSON";
            this.metroRadioButtonJSON.Size = new System.Drawing.Size(139, 15);
            this.metroRadioButtonJSON.TabIndex = 3;
            this.metroRadioButtonJSON.Text = "Use JSON Serialization";
            this.metroRadioButtonJSON.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroRadioButtonJSON);
            this.groupBox1.Controls.Add(this.metroRadioButtonXML);
            this.groupBox1.Location = new System.Drawing.Point(20, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(887, 63);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(440, 25);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(136, 19);
            this.metroLabel5.TabIndex = 4;
            this.metroLabel5.Text = "Serialization Method :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.metroButtonPublish);
            this.groupBox2.Controls.Add(this.metroLabel3);
            this.groupBox2.Controls.Add(this.metroTextBoxPostContent);
            this.groupBox2.Controls.Add(this.metroTextBoxPostName);
            this.groupBox2.Controls.Add(this.metroLabel2);
            this.groupBox2.Location = new System.Drawing.Point(20, 131);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 374);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // metroButtonPublish
            // 
            this.metroButtonPublish.Location = new System.Drawing.Point(6, 332);
            this.metroButtonPublish.Name = "metroButtonPublish";
            this.metroButtonPublish.Size = new System.Drawing.Size(408, 30);
            this.metroButtonPublish.TabIndex = 5;
            this.metroButtonPublish.Text = "Publish";
            this.metroButtonPublish.Click += new System.EventHandler(this.metroButtonPublish_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(6, 47);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(90, 19);
            this.metroLabel3.TabIndex = 3;
            this.metroLabel3.Text = "Post Content :";
            // 
            // metroTextBoxPostContent
            // 
            this.metroTextBoxPostContent.Location = new System.Drawing.Point(6, 69);
            this.metroTextBoxPostContent.Multiline = true;
            this.metroTextBoxPostContent.Name = "metroTextBoxPostContent";
            this.metroTextBoxPostContent.PromptText = "Type here the content of the post";
            this.metroTextBoxPostContent.Size = new System.Drawing.Size(408, 250);
            this.metroTextBoxPostContent.TabIndex = 2;
            // 
            // metroTextBoxPostName
            // 
            this.metroTextBoxPostName.Location = new System.Drawing.Point(91, 16);
            this.metroTextBoxPostName.Name = "metroTextBoxPostName";
            this.metroTextBoxPostName.PromptText = "Type here the name of the post";
            this.metroTextBoxPostName.Size = new System.Drawing.Size(323, 23);
            this.metroTextBoxPostName.TabIndex = 1;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(6, 17);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(80, 19);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Post Name :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.metroLabel4);
            this.groupBox3.Controls.Add(this.metroTextBoxSerializedPosts);
            this.groupBox3.Location = new System.Drawing.Point(460, 131);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(447, 374);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(6, 17);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(105, 19);
            this.metroLabel4.TabIndex = 4;
            this.metroLabel4.Text = "Serialized Posts :";
            // 
            // metroTextBoxSerializedPosts
            // 
            this.metroTextBoxSerializedPosts.Location = new System.Drawing.Point(6, 47);
            this.metroTextBoxSerializedPosts.Multiline = true;
            this.metroTextBoxSerializedPosts.Name = "metroTextBoxSerializedPosts";
            this.metroTextBoxSerializedPosts.PromptText = "There is no published posts.";
            this.metroTextBoxSerializedPosts.ReadOnly = true;
            this.metroTextBoxSerializedPosts.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.metroTextBoxSerializedPosts.Size = new System.Drawing.Size(435, 315);
            this.metroTextBoxSerializedPosts.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Made with ❤ by Bophar Hen";
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroComboBoxSocialNetwork);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "GUI";
            this.Padding = new System.Windows.Forms.Padding(23, 69, 23, 23);
            this.Resizable = false;
            this.Text = "SocialHenwork";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox metroComboBoxSocialNetwork;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroRadioButton metroRadioButtonXML;
        private MetroFramework.Controls.MetroRadioButton metroRadioButtonJSON;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox metroTextBoxPostContent;
        private MetroFramework.Controls.MetroTextBox metroTextBoxPostName;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroButton metroButtonPublish;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox metroTextBoxSerializedPosts;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private System.Windows.Forms.Label label1;
    }
}

