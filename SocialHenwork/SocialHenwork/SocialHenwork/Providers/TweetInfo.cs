﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialHenwork.Providers
{
    public class TweetInfo
    {
        public TweetInfo(String str, long idd, DateTime date, String addr)
        {
            TweetContent = str;
            Id = idd;
            DateCrea = date;
            Url = addr;
        }
        
        public TweetInfo()
        {

        }

        private String tweetContent;
        public String TweetContent
        {
            get => tweetContent;
            set => tweetContent = value;
        }

        private long id;
        public long Id
        {
            get => id;
            set => id = value;
        }

        private DateTime dateCrea;
        public DateTime DateCrea
        {
            get => dateCrea;
            set => dateCrea = value;
        }

        private String url;
        public String Url
        {
            get => url;
            set => url = value;
        }
    }
}
