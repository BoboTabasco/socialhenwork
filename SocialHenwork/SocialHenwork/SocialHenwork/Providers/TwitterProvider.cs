﻿using MetroFramework.Forms;
using SocialHenwork.General;
using SocialHenwork.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace SocialHenwork.Providers
{
    public class TwitterProvider : AProvider
    {
        private static long USERID = 1061563669048426496;

        public TwitterProvider()
        {
            //Initialisation de l'API Twitter pour qu'il utilise le compte @TongjiT (Tongji Test)
            Auth.SetUserCredentials("G3kXJOJ48gRHj5k0Igv2yGcMg", "6dYTjZUdJYsfg23U2V3Xv9K3GVbWYrW0Vx3FIoFCsv9ZAVEyd6", "1061563669048426496-F2eHnDBh5FI2iREnTs4WmiUVFM4cBT", "pMzGFWPsEaJFESl3ddgz6MywgpCGktMITQt0zqkvlB1dA");
        }

        public override string GetSerializedPosts()
        {
            var lastTweets = Timeline.GetUserTimeline(USERID, 200).ToArray();
            var allTweets = new List<ITweet>(lastTweets);
            var beforeLast = allTweets;

            while (lastTweets.Length > 0 && allTweets.Count <= 3200)
            {
                var idOfOldestTweet = lastTweets.Select(x => x.Id).Min();
                Console.WriteLine($"Oldest Tweet Id = {idOfOldestTweet}");

                var numberOfTweetsToRetrieve = allTweets.Count > 3000 ? 3200 - allTweets.Count : 200;
                var timelineRequestParameters = new UserTimelineParameters
                {
                    // MaxId ensures that we only get tweets that have been posted 
                    // BEFORE the oldest tweet we received
                    MaxId = idOfOldestTweet - 1,
                    MaximumNumberOfTweetsToRetrieve = numberOfTweetsToRetrieve
                };

                lastTweets = Timeline.GetUserTimeline(USERID, timelineRequestParameters).ToArray();
                allTweets.AddRange(lastTweets);
            }

            List<TweetInfo> tweets = new List<TweetInfo>();
            foreach(ITweet tw in allTweets)
            {
                TweetInfo info = new TweetInfo(tw.FullText, tw.Id, tw.CreatedAt, tw.Url);
                tweets.Add(info);
            }

            if (Static.USING_XML)
                return XMLUtils.SerializeObject(tweets);
            else
                return JSONUtils.SerializeObject(tweets);
        }

        public override void Publish(object post)
        {
            if (post == null || post.GetType() != typeof(String))
                throw new Exception("Cannot publish null object or invalid string.");

            String str = post as String;
            if (String.IsNullOrEmpty(str))
            {
                MessageBox.Show("Cannot tweet and empty string !");
                return;
            }
            if (str.Length > 140)
            {
                MessageBox.Show("Cannot tweet " + str.Length + " characters !");
                return;
            }
            Tweet.PublishTweet(str);
        }
    }
}
