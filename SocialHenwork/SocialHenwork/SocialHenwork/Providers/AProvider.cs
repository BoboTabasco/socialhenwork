﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialHenwork.Providers
{
    /// <summary>
    /// Classe abstraite qui possède toutes les méthodes obligatoires d'un réseau social
    /// (pour notamment publier et lister les posts)
    /// </summary>
    public abstract class AProvider
    {
        public abstract void Publish(object post);
        public abstract String GetSerializedPosts();
    }
}
