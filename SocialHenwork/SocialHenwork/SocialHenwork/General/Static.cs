﻿using SocialHenwork.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialHenwork.General
{
    public static class Static
    {
        public static TwitterProvider TWITTER = new TwitterProvider();

        //Détermine si le programme utilise du XML ou du JSON
        public static bool USING_XML = true;
    }
}
